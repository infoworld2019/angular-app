import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ItemsComponent} from './items/items.component';
import {ItemDetailComponent} from './items/item-detail.component';
import {LoginComponent} from './auth/login.component';


const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {path: 'item', component: ItemsComponent},
  {path: 'item/:id', component: ItemDetailComponent},
  {path: 'auth/login', component: LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
