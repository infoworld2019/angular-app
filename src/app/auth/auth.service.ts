import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

const serverUrl = 'http://localhost:3000';
const authUrl = `${serverUrl}/api/auth`;

interface AuthResponse {
  token: string;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    // 'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) {
  }

  login(username: string, password: string): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(`${authUrl}/login`, { username, password }, httpOptions)
      .pipe(
        tap(res => localStorage.setItem('token', res.token))
      );
  }
}
