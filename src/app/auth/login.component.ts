import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-auth-login',
  template: `
    <div>
      <div>
        <input type="text" placeholder="username" [(ngModel)]="username">
      </div>
      <div>
        <input type="password" placeholder="password" [(ngModel)]="password">
      </div>
      <button (click)="login()">Login</button>
    </div>
  `
})
export class LoginComponent implements OnInit, OnDestroy {
  protected error: Error;
  private subscriptions: Subscription[] = [];
  username: string;
  password: string;

  constructor(private authService: AuthService) {
    console.log('login-constructor');
  }

  ngOnInit() {
    console.log('login-init');
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  login() {
    this.subscriptions.push(
      this.authService.login(this.username, this.password)
        .subscribe(result => {
            console.log(result);
          },
          error => {
            console.log(error);
          })
    );
  }
}
