import {Injectable} from '@angular/core';
// import {ITEMS} from './mock-items';
import {Item} from './item';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
// import WebSocket from 'ws';
declare var WebsSocket: any;

const serverUrl = 'http://localhost:3000';
const itemUrl = `${serverUrl}/api/item`;

const httpOptions = token => ({
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  })
});

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  cachedItems: Item[];
  itemSource: BehaviorSubject<Item[]>;

  constructor(private httpClient: HttpClient) {
    this.itemSource = new BehaviorSubject<Item[]>(this.cachedItems);
    const ws: any = new WebSocket('ws://localhost:3000');
    ws.onopen = () => {
      console.log('WS connection opened!');
    };

    ws.onmessage = (event) => {
      console.log('Incoming data: ', event.data);
      const item = JSON.parse(event.data);
      const items = this.itemSource.getValue();
      if (item) {
        items.push(item);
        this.itemSource.next(items);
      } else {
        this.itemSource.next([item]);
      }
    };
  }

  getItems(): Observable<Item[]> {
    this.httpClient.get<Item[]>(itemUrl, httpOptions(localStorage.getItem('token')))
      .subscribe(items => this.itemSource.next(items));
    return this.itemSource.asObservable();
  }

  getItem(id: number): Observable<Item> {
    if (this.cachedItems) {
      const item = this.cachedItems.find(it => it.id === id);
      if (item) {
        return of(item);
      }
    }
    return this.httpClient.get<Item>(`${itemUrl}/${id}`);
  }
}
