import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Item} from './item';
// import {ITEMS} from './mock-items';
import {ActivatedRoute} from '@angular/router';
import {ItemService} from './item.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-item-detail',
  template: `
    <h1>Item details</h1>
    <div *ngIf="error">
      <p>Service unavailable!</p>
      <button (click)="retry()">Retry</button>
    </div>
    <div *ngIf="!item">Loading...</div>
    <div *ngIf="item">
      <div>{{item.id}}</div>
      <div>{{item.text}}</div>
      <label>edit text:
        <input [(ngModel)]="item.text"/>
      </label>
    </div>

  `
})
export class ItemDetailComponent implements OnInit, OnDestroy {

  @Input() item: Item;
  protected error: Error;
  private id: number;
  private subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    // private location: Location,
    private itemService: ItemService
  ) {
    console.log('item-detail: construct');
  }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.loadItem();

  }

  loadItem() {
    this.error = null;
    this.subscriptions.push(this.itemService.getItem(this.id)
      .subscribe(item => this.item = item,
        error => this.error = error));
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());

  }

  retry() {
    this.loadItem();
  }
}
