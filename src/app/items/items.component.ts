import {Component, OnDestroy, OnInit} from '@angular/core';
import {Item} from './item';
import {ItemService} from './item.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-items',
  template: `
    <div>
      <div *ngIf="error">
        <p>Service unavailable!</p>
      <button (click)="retry()">Try again</button>
      </div>
      <div *ngIf="!items">Loading...</div>
      <!--{{item.text}}-->
      <ul>
        <li *ngFor="let item of items" (click)="onSelectItem(item)">
          <a routerLink="/item/{{item.id}}">{{item.id}} {{item.text}}</a>
        </li>
      </ul>
      <!--<app-item-detail *ngIf="selectedItem" [item]= "selectedItem"></app-item-detail>-->
    </div>
  `
})
export class ItemsComponent implements OnInit, OnDestroy {

  // item: Item = {
  //   id: 3,
  //   text: 'item-3'
  // };
  selectedItem: Item;
  private intervalId: number;
  // private itemService: ItemService;
  items: Item[];
  protected error: Error;
  private subscriptions: Subscription[] = [];

  constructor(private itemService: ItemService) {
    // this.itemService = itemService;
    console.log('constructor');
  }

  loadItems() {
    this.error = null;
    this.subscriptions.push(this.itemService.getItems().subscribe(
      items => this.items = items,
        error => {
        this.error = error;
      }));
  }
  // alt apel prin care subscriem la erorile posibile care se intampla in interiorul serviciului care face rost de date

  // read about behaviour subjects, observables etc
  ngOnInit() {
    console.log('init');
    this.loadItems();

    // this.intervalId = setInterval(() => {
    //   // this.item.text += '!';
    //   // this.items.push({id: this.items.length, text: 'newItem'});
    //   this.selectedItem = null;
    // }, 2000);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    clearInterval(this.intervalId);
  }

  onSelectItem(item) {
    console.log(item);
    this.selectedItem = item;
  }

  retry() {
    this.loadItems();
  }
}
