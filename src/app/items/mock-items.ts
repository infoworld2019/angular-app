import {Item} from './item';

export const ITEMS: Item[] = [
  {id: 1, text: 'item-1'},
  {id: 2, text: 'item-2'}
];
